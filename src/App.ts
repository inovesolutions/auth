import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as Jwt from 'jsonwebtoken';
import * as cors from 'cors';
import { AuthorizationController } from './authorization/Authorization.controller';
import { Database } from './database/Database';
import { TokenController } from './token/Token.controller';

class App {

  public express;

  constructor () {
    this.express = express();
    this.mountRoutes();
    Database.connect();
  }

  private mountRoutes (): void {

    const router = express.Router();

    router.get('/', (request:express.Request, response:express.Response) => {
      response.send(`ok`);
    });

    router.post('/authorization', AuthorizationController.create);
    router.post('/token', TokenController.create);
    router.put('/token/:refreshToken', TokenController.update);
    router.get('/:clientId/tokens/:accessToken', TokenController.verify);

    this.express.use(cors());
    this.express.use(bodyParser.urlencoded({ extended: false }));
    this.express.use(bodyParser.json());

    this.express.use('/api', (request:express.Request, response:express.Response, next:express.NextFunction) => {
      let token = request.body.token || request.query.token || request.headers['authorization'];
      if(token){
        Jwt.verify(token, request.app.get('secretKey'), (error, decoded) => {
          if(error) return response.status(401).json({reason: 'Unauthorized'});
          request.app.set('user', decoded);
          next();
        })
      } else response.status(401).json({reason: 'Unauthorized'});
    });
    
    this.express.use('/', router);
  }
}

export default new App().express;
