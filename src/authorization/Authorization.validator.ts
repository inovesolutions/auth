import * as Revalidator from 'revalidator';
import { AuthorizationRequest } from './Authorization.controller';

export class AuthorizationValidator {
  private authorizationSchema:Revalidator.JSONSchema<any> = {
    properties: {
      responseType: {
        type: 'string',
        allowEmpty: false,
        required: true,
        conform: function (v) {
          if (v === 'code') return true;
          return false;
        }
      },
      clientId: {
        type: 'string',
        allowEmpty: false,
        required: true
      },
      prefix: {
        type: 'string',
        allowEmpty: false,
        required: true
      }
    }
  };

  public validate(authorizationRequest:AuthorizationRequest):Revalidator.IReturnMessage{
    return Revalidator.validate(authorizationRequest, this.authorizationSchema);
  }
}