import { Request, Response } from "express";
import { AuthorizationValidator } from "./Authorization.validator";
import { Database } from "../database/Database";
import * as ShortId from 'shortid';
import * as Crypto from 'crypto';

export class AuthorizationController {
  public static create(request:Request, response:Response){

    let authorizationRequest:AuthorizationRequest = request.body;
    let validation = new AuthorizationValidator().validate(authorizationRequest);
    if (!validation.valid) return response.status(400).json(validation.errors);

    let table = Database.getTable(authorizationRequest.prefix, 'projects');
    Database.runOperation<Project>(table.get(authorizationRequest.clientId))
    .then(project => {
      if (project) {
        // if (!project.domains.find(d => d === request.headers.origin)) {
        //   return response.status(401).send({reason: 'Domain unauthorized'});
        // }
        authorizationRequest.code = ShortId.generate();
        authorizationRequest.timestamp = + new Date();
        const secretKey = request.app.get('secretKey');
        const cipher = Crypto.createCipher('aes192', secretKey);
        let code = cipher.update(JSON.stringify(authorizationRequest), 'utf8', 'hex');
        code += cipher.final('hex');

        if (authorizationRequest.redirectUri) {
          let uri = `${authorizationRequest.redirectUri}?code=${code}&state=${authorizationRequest.state || ''}`
          response.status(302).redirect(uri);
        } else {
          response.status(200).json({
            code, state: authorizationRequest.state || null
          });
        }
      } else response.status(401).send('Unauthorized');
    })
    .catch(() => response.status(401).json('Unauthorized'))
  }
}

export class AuthorizationRequest {
  public responseType:string;
  public prefix:string;
  public clientId:string;
  public redirectUri?:string;
  public scope?:string;
  public state?:string;
  public code?:string;
  public timestamp:number;
}

export class Project {
  public id:string;
  public name:string;
  public owner?:string;
  public authRedirectUri?:string;
  public domains:Array<string> = [];
}