import { Request, Response } from "express";
import { TokenValidator } from "./Token.validator";

import * as Crypto from 'crypto';
import * as JWT from 'jsonwebtoken';
import * as moment from 'moment';

import { AuthorizationRequest } from "../authorization/Authorization.controller";
import { Database } from "../database/Database";
import { AnyObject } from "../common/AnyObject.interface";

const EXPIRES_IN = 3600;
export class TokenController {
  public static create(request: Request, response: Response) {
    let tokenRequest: TokenRequest = request.body;
    if (tokenRequest.grantType === 'authorization_code') {
      let validation = new TokenValidator().validate(tokenRequest);
      if (!validation.valid) return response.status(400).json(validation.errors);

      let secretKey = request.app.get('secretKey');
      const decipher = Crypto.createDecipher('aes192', secretKey);
      let decrypted = decipher.update(tokenRequest.code, 'hex', 'utf8');
      decrypted += decipher.final('utf8');
      let authorizationRequest: AuthorizationRequest = JSON.parse(decrypted);

      let invalid = false
      if (authorizationRequest.clientId !== tokenRequest.clientId) invalid = true
      if (authorizationRequest.redirectUri && authorizationRequest.redirectUri !== tokenRequest.redirectUri) invalid = true
      if (invalid) return response.status(401).send('Unauthorized')

      let sequence = Database.getTable(authorizationRequest.prefix, 'projects')
        .filter({ id: tokenRequest.clientId, secretKey: tokenRequest.clientSecret });
      Database.runSequence(sequence)
        .then(result => result.toArray().then(value => {
          if (value.length) {
            let tokenResponse: TokenResponse = new TokenResponse();
            let tokenInfo: TokenInfo = {
              clientId: tokenRequest.clientId,
              timestamp: +new Date(),
              tokenType: tokenRequest.grantType,
              state: authorizationRequest.state
            };
            tokenResponse.accessToken = JWT.sign(tokenInfo, secretKey);
            tokenResponse.tokenType = tokenRequest.grantType;
            tokenResponse.refreshToken = Buffer.from(JSON.stringify(tokenInfo)).toString('base64');
            response.status(200).json(tokenResponse);
          } else response.status(401).send('Unauthorized')
        })).catch(() => response.status(401).send('Unauthorized'))
    } else response.status(401).send('Unauthorized');
  }

  public static update(request:Request, response:Response) {
    let secretKey = request.app.get('secretKey');
    let tokenResponse:TokenResponse = new TokenResponse();
    let tokenInfo:TokenInfo = JSON.parse(Buffer.from(request.params.refreshToken, 'base64')
    .toString('ascii'));
    tokenInfo.timestamp = +new Date();
    tokenResponse.accessToken = JWT.sign(tokenInfo, secretKey);
    tokenResponse.tokenType = tokenInfo.tokenType;
    tokenResponse.refreshToken = request.params.refreshToken;
    response.status(200).json(tokenResponse);
  }

  public static verify(request:Request, response:Response) {
    let clientId = request.params.clientId;
    let secretKey = request.app.get('secretKey');
    let accessToken = request.params.accessToken;

    let tokenInfo:TokenInfo = JWT.verify(accessToken, secretKey) as TokenInfo;
    if (!tokenInfo) return response.status(401).send('Unauthorized');

    if (tokenInfo.clientId !== clientId) return response.status(401).json({
      reason: 'The property clientId not matched'
    });

    if(moment(tokenInfo.timestamp).isBefore(moment().subtract('seconds', EXPIRES_IN))) {
      return response.status(401).json({
        reason: 'AccessToken was expired'
      });
    }
    return response.status(200).json({valid: true});
  }
}

export class TokenRequest {
  public clientId:string;
  public clientSecret:string;
  public grantType:string;
  public code?:string;
  public username?:string;
  public password?:string;
  public redirectUri?:string;
}

export class TokenResponse {
  public accessToken:string;
  public tokenType:string;
  public expiresIn:number = EXPIRES_IN;
  public refreshToken:string;
}

export class TokenInfo implements AnyObject {
  clientId:string; 
  timestamp:number;
  tokenType:string;
  state:string;
}