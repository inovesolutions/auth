import * as Revalidator from 'revalidator';
import { TokenRequest } from './Token.controller';

export class TokenValidator {
  private tokenSchema:Revalidator.JSONSchema<any> = {
    properties: {
      grantType: {
        type: 'string',
        allowEmpty: false,
        required: true,
        conform: function (v) {
          if (v === 'authorization_code') return true;
          return false;
        }
      },
      clientId: {
        type: 'string',
        allowEmpty: false,
        required: true
      },
      clientSecret: {
        type: 'string',
        allowEmpty: false,
        required: true
      },
      code: {
        type: 'string',
        allowEmpty: false,
        required: true
      },
      username: {
        type: 'string',
        allowEmpty: false,
        required: true
      },
      password: {
        type: 'string',
        allowEmpty: false,
        required: true
      }
    }
  };

  public validate(tokenRequest:TokenRequest):Revalidator.IReturnMessage{
    if (tokenRequest.grantType === 'authorization_code') {
      delete this.tokenSchema.properties.username
      delete this.tokenSchema.properties.password
    } else if (tokenRequest.grantType === 'password') {
      delete this.tokenSchema.properties.clientId
      delete this.tokenSchema.properties.clientSecret
      delete this.tokenSchema.properties.code
    }
    return Revalidator.validate(tokenRequest, this.tokenSchema);
  }
}