import * as RethinkDB from 'rethinkdb';

export class Config {

  public secret = 'fGJwB6wPMT';
  public connectionOptions:RethinkDB.ConnectionOptions = {};

  constructor() {
    this.connectionOptions.host = 'localhost';
    this.connectionOptions.port = 28045;
    // this.connectionOptions.user = 'admin';
    // this.connectionOptions.password = 'console';
  }
}